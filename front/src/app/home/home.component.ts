import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Emitters } from '../emitters/emitters';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  public doughnutChartLabels = ['Entrés', 'Sorties'];
  public doughnutChartData = [{
    data: [120, 150],
    backgroundColor: ["#105A6D", "#48879b"]
  }];
  public doughnutChartType = 'doughnut';

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels = ['2015', '2016', '2017', '2018', '2019', '2020'];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [59, 80, 81, 56, 55, 40], 
      label: 'CA',
      backgroundColor: ["#3d9fb8", "#48879b", "#105A6D", "#02303b", "#97cbd8", "#97cbd8", "#97cbd8"] },
  ];
  authenticated = false;
  message = '';
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    Emitters.authEmitter.subscribe(
      (auth: boolean) => {
        this.authenticated = auth;
      }
    );
    this.http.get('http://localhost:8000/api/user', {withCredentials: true}).subscribe(
      (res: any) => {
        this.message = `Bonjour, ${res.first_name}`;
        Emitters.authEmitter.emit(true);
      },
      err => {
        this.message = 'Vous nêtes pas connecté';
        Emitters.authEmitter.emit(false);
      }
    );
  }

}
