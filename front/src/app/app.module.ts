import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DetailsProductComponent } from './details-product/details-product.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { SidebarComponent } from './sidebar/sidebar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {MatCardModule, MatButtonModule} from '@angular/material';
import { PageProduitsComponent } from './page-produits/page-produits.component';
import { ChartsModule, ThemeService } from 'ng2-charts';
import {LoginComponent} from './login/login.component';
import { FeatherModule } from 'angular-feather';
import { ShoppingCart, BarChart2, Home, File, LogOut, LogIn } from 'angular-feather/icons';

const icons = {
  ShoppingCart,
  BarChart2,
  Home,
  File,
  LogOut,
  LogIn,
};

@NgModule({
  declarations: [
    AppComponent,
    DetailsProductComponent,
    HomeComponent,
    SidebarComponent,
    PageProduitsComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    BrowserAnimationsModule,
    NgMultiSelectDropDownModule.forRoot(),
    MatCardModule, MatButtonModule,
    ChartsModule,
    ReactiveFormsModule,
    FeatherModule.pick(icons),
  ],
  exports: [
    FeatherModule
  ],
  providers: [ThemeService],
  bootstrap: [AppComponent],
  entryComponents: [],
  schemas: []
})
export class AppModule { }
