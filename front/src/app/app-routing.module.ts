import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailsProductComponent } from './details-product/details-product.component';
import { PageProduitsComponent } from './page-produits/page-produits.component';
import { HomeComponent } from './home/home.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LoginComponent } from './login/login.component';
//import { HomeComponent } from './home/home.component';


const routes: Routes = [
  
  {
    path: '', component: SidebarComponent, children: [
      {path: 'login', component: LoginComponent},
      { path: 'detailsProduit', component: DetailsProductComponent },
      { path: 'home', component: HomeComponent },
      { path: 'listProduits', component: PageProduitsComponent},
    ],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
