import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  urlApi: string = "http://localhost:8000/";

  constructor(private http: HttpClient) {
   //this.urlApi = "../assets/data/products.json";  
  }

  getData(){
    console.log( this.http.get(this.urlApi + `infoproducts/`));
    console.log(this.urlApi);
    return this.http.get(this.urlApi + `infoproducts/`);
  }

  setData(urlModif){
    console.log(this.http.get(this.urlApi + urlModif));
    return this.http.get(this.urlApi + urlModif);
  }
}
