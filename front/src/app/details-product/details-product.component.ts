import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductsService } from '../services/products.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { Emitters } from '../emitters/emitters';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-details-product',
  templateUrl: './details-product.component.html',
  styleUrls: ['./details-product.component.css']
})
export class DetailsProductComponent implements OnInit {

  dropdownList = [];
  listDetailProduct = [];
  dropdownSettings:IDropdownSettings;

  products;
  product;
  authenticated = false;
  message = '';
  listModif;
  urlChangeDiscount = ""

  constructor(public productsService : ProductsService, private http: HttpClient) {
    this.products = [];
    console.log(this.products);
    this.product = [];
    this.listModif = []
  }

  ngOnInit() {
    Emitters.authEmitter.subscribe(
      (auth: boolean) => {
        this.authenticated = auth;
      }
    );
    this.http.get('http://localhost:8000/api/user', {withCredentials: true}).subscribe(
      (res: any) => {
        this.message = `Hi ${res.first_name}`;
        Emitters.authEmitter.emit(true);
      },
      err => {
        this.message = 'Vous nêtes pas connecté';
        Emitters.authEmitter.emit(false);
      }
    );
    this.dropdownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 1,
      allowSearchFilter: true
    };
    this.getDataFromDB();
  }

  getProductCollection(p){
      console.log(p)
  }

  onItemSelect(product: any) {
    product = this.getProductId(product.id)
    this.product = product
  }

  getProductId(id){
    for(let p of this.products){
      if(p.id == id){
        this.product = p;
        this.listDetailProduct.splice(p,1);
        this.listDetailProduct.push(p);
      }
    }
  }

  setProductPromo(inputValue){
    console.log("input value : " + inputValue);
    for (let p of this.listDetailProduct){
      this.urlChangeDiscount = "putondiscount/" + p.id + "/" + inputValue
    }
    this.setDataToDB(this.urlChangeDiscount)
    this.getDataFromDB();
  }

  onSelectAll(product: any) {
    console.log(product);
  }


  setDataToDB(url){
    this.productsService.setData(url).subscribe(res => {
      this.listModif = res;
      console.log(this.listModif);
      this.getDataFromDB();
      window.location.reload();
    },
    (err) => {
      alert('failed to change discount');
    });
  }

  getDataFromDB(){
    this.productsService.getData().subscribe(res => {
      this.products = res;
      this.getProductCollection(this.products);
    },
    (err) => {
      alert('failed loading json data');
    });
  }

}
