import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Emitters } from '../emitters/emitters';
import { ProductsService } from '../services/products.service';

@Component({
  selector: 'app-page-produits',
  templateUrl: './page-produits.component.html',
  styleUrls: ['./page-produits.component.css']
})
export class PageProduitsComponent implements OnInit {

  products;
  product;
  listFish = []; // category = 0
  listSeaFood = []; // category = 1
  listShellFish = []; // category = 2
  listCategory = [0, 1, 2];
  listNewProductsInfo = [];
  inputValueFullID;
  inputValueID;
  inputValue;
  authenticated = false;
  message = 'You are not logged in';
  urlModif = "";
  listModif; //list use to check modif in console

  constructor(public productsService: ProductsService, private http: HttpClient) {
    this.products = [];
  }

  ngOnInit() {
    Emitters.authEmitter.subscribe(
      (auth: boolean) => {
        this.authenticated = auth;
      }
    );
    this.http.get('http://localhost:8000/api/user', {withCredentials: true}).subscribe(
      (res: any) => {
        this.message = `Bonjour ${res.first_name}`;
        Emitters.authEmitter.emit(true);
      },
      err => {
        this.message = 'Vous nêtes pas connecté';
        Emitters.authEmitter.emit(false);
      }
    );
    this.getDataFromDB();// get data fron DB
  }

  getProduct() {
    return this.products, this.listCategory;
  }

  // Event that handle button click to update stocks and product discount
  onClick(event) {
    console.log(this.listNewProductsInfo);
    //add poducts changes to new list
    for (let p of this.products) {
      for (let n of this.listNewProductsInfo) {
        if (p.id == n.id && p.category == n.category) {
          if (0 != n.discount) {
            this.urlModif = "putondiscount/" + n.id + "/" + n.discount 
            console.log("url des modifications : " + this.productsService.setData(this.urlModif))
            //set data to backend db using get method. Data is set in urls
            this.setDataToDB(this.urlModif)
          }
          //besoin de faire un check sur la quantité
          if (n.quantityInStock != n.quantityInStock + p.quantityInStock) {
            //n.quantityInStock = n.quantityInStock+ p.quantityInStock;
            this.urlModif = "incrementStock/" + n.id + "/" + n.quantityInStock 
            console.log("url des modification : " + this.urlModif)
            this.setDataToDB(this.urlModif)
          }
        }
      }
      // body = info à envoyer
      // utiliser les urls du back pour les modifications

      // this.http.post("http://localhost:8080/user/all").subscribe((data) => {});
    }
    //mettre les valeurs des input à vide après avoir appuyer sur 'sauve'?
    console.log(this.listNewProductsInfo);
    console.log(this.products);
  }

  onKey(event: any) {
    this.inputValueFullID = event.currentTarget.id;
    this.inputValueID = this.inputValueFullID.match(/\d+/g).join(''); //remove all the latters
    for (let p of this.products) {
      if (this.inputValueID == p.id && !this.listNewProductsInfo.includes(p.name)) {
        this.listNewProductsInfo.push(p);
      }
      // allow to change values that are already been changed by user
      // or multiple digit numbers (refreshed at each user input)
      if (this.listNewProductsInfo.includes(p) && this.inputValueID == p.id) {
        for (let n of this.listNewProductsInfo) {
          if (n.id == p.id && n.id == this.inputValueID) {
            if (this.inputValueFullID.includes('Discount')) { // check to see if user delete previous input for the current item
              if (event.target.value == '') {
                console.log('Empty input');
                n.discount = 0;
              } else {
                n.discount = parseFloat(event.target.value);
                console.log(event.target.value);
              }
            }
            //  update quantity with the new one
            //  does not add quantity because value would change at each user input
            //  so that avoid unwanted quantity for multiple digits numbers
            //  quantity added to new list with products's list data and listNewProductsInfo
            //  when pressing 'save' button to new list 
            else {
              if (event.target.value == '') {
                n.quantityInStock = 0;
              } else {
                n.quantityInStock = parseFloat(event.target.value);
                console.log(event.target.value);
              }
            }
          }
        }
      }
    }
  }

  getDataFromDB(){
    this.productsService.getData().subscribe(res => {
      this.products = res;
      this.getProduct();
      // console.log(this.products);
    },
    (err) => {
      alert('failed loading json data');
    });
  }

  setDataToDB(url){
    this.productsService.setData(url).subscribe(res => {
      this.listModif = res;
      console.log(this.listModif);
      this.getDataFromDB();
    },
    (err) => {
      alert('failed to increment stock');
    });
  }


}