from django.db import models

class ProductStock(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    tigID = models.IntegerField(default='-1')
    quantityInStock = models.IntegerField(default='0')
    quantity_sold = models.IntegerField(default='0')
    newprice = models.FloatField(default=0) 
    price_on_sale = models.FloatField(default=0)
    discount = models.FloatField(default=0)
    sale = models.BooleanField(default=True)

    
    class Meta:
        ordering = ('tigID',)

