from django.urls import path, register_converter, re_path
from myStockProduct import views

class FloatUrlParameterConverter: #Une class python qui respecte le besoin django pour prendre en charge les flottants
    regex = '[0-9]+\.?[0-9]+'

    def to_python(self, value):
        return float(value)

    def to_url(self, value):
        return str(value)

register_converter(FloatUrlParameterConverter, 'floatsss')



urlpatterns = [
    path('infoproducts/', views.ProductsStock.as_view()),
    path('infoproduct/<int:pk>/', views.ProductStockDetail.as_view()),
    path('incrementStock/<int:pk>/<int:qty>/', views.IncrStock.as_view()),
    path('decrementStock/<int:pk>/<int:qty>/', views.DecrStock.as_view()),
    path('putondiscount/<int:pk>/<floatsss:newprice>/', views.PutOnSale.as_view()),
]
