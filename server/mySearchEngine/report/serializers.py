from rest_framework.serializers import ModelSerializer
from report.models import AchatProduct, VentePrduct

class AchatProdSerializer(ModelSerializer):
    class Meta:
        model = AchatProduct
        fields = ('tigID', 'price', 'quantity_buy', 'date')
        
class VenteProdSerializer(ModelSerializer):
    class Meta:
        model = VentePrduct
        fields = ('tigID', 'price', 'quantity_sold', 'date', 'sale')
