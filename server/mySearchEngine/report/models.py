from django.db import models

# Create your models here.


class AchatProduct(models.Model):
    tigID = models.IntegerField(default='-1')
    date = models.BigIntegerField()
    quantity_buy = models.IntegerField()
    price = models.FloatField()
    class Meta:
        ordering = ('tigID',)


class VentePrduct(models.Model):
    tigID = models.IntegerField(default='-1')
    date = models.BigIntegerField()
    quantity_sold = models.IntegerField()
    price = models.FloatField()
    sale = models.FloatField(default=0.0)
    class Meta:
        ordering = ('tigID',)
