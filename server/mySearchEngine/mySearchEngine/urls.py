from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from api.views import UserViewSet, LoginView, UserView, LogoutView

router = routers.DefaultRouter()
router.register('users', UserViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('mytig.urls')),
    path('', include('myImageBank.urls')),
    path('', include('myStockProduct.urls')),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls')),
    path('api/login/', LoginView.as_view()),
    path('api/user/', UserView.as_view()),
    path('api/logout/', LogoutView.as_view())
]
